package main

import (
	"encoding/json"
	"fmt"
	"github.com/caarlos0/env"
	"github.com/streadway/amqp"
	"gopkg.in/gomail.v2"
	"io"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

type RabbitMessage struct {
	Html        string       `json:"html"`
	Cc          string       `json:"cc"`
	Bcc          string       `json:"bcc"`
	To          string       `json:"to"`
	ReplyTo     string       `json:"replyTo"`
	Subject     string       `json:"subject"`
	Attachments []Attachment `json:"attachments"`
}

type Attachment struct {
	Content  string `json:"content"`
	Filename string `json:"filename"`
}

type Config struct {
	AmqpConnect            string `env:"RABBIT_MAILER_AMQP_CONNECT,required"`
	AmqpQueueName          string `env:"RABBIT_MAILER_AMQP_QUEUE_NAME,required"`
	AmqpExchangeName       string `env:"RABBIT_MAILER_AMQP_EXCHANGE_NAME,required"`
	AmqpExchangeType       string `env:"RABBIT_MAILER_AMQP_EXCHANGE_TYPE,required"`
	AmqpQueueIsDurable     bool   `env:"RABBIT_MAILER_AMQP_QUEUE_IS_DURABLE,required"`
	AmqpExchangeIsDurable  bool   `env:"RABBIT_MAILER_AMQP_EXCHANGE_IS_DURABLE,required"`
	AmqpPrefetchCount      int    `env:"RABBIT_MAILER_AMQP_PREFETCH_COUNT,required"`
	AmqpPrefetchSize       int    `env:"RABBIT_MAILER_AMQP_PREFETCH_SIZE,required"`
	SmtpHost               string `env:"RABBIT_MAILER_SMTP_HOST,required"`
	SmtpPort               int    `env:"RABBIT_MAILER_SMTP_PORT,required"`
	SmtpFromEmail          string `env:"RABBIT_MAILER_SMTP_FROM_EMAIL,required"`
	AmqpQueueAutoDelete    bool   `env:"RABBIT_MAILER_AMQP_QUEUE_AUTO_DELETE" envDefault:"false"`
	AmqpQueueExclusive     bool   `env:"RABBIT_MAILER_AMQP_QUEUE_EXCLUSIVE" envDefault:"false"`
	AmqpQueueNoWait        bool   `env:"RABBIT_MAILER_AMQP_QUEUE_NO_WAIT" envDefault:"false"`
	AmqpExchangeAutoDelete bool   `env:"RABBIT_MAILER_AMQP_EXCHANGE_AUTO_DELETE" envDefault:"false"`
	AmqpExchangeInternal   bool   `env:"RABBIT_MAILER_AMQP_EXCHANGE_INTERNAL" envDefault:"false"`
	AmqpExchangeNoWait     bool   `env:"RABBIT_MAILER_AMQP_EXCHANGE_NO_WAIT" envDefault:"false"`
	AmqpQosGlobal          bool   `env:"RABBIT_MAILER_AMQP_QOS_GLOBAL" envDefault:"false"`
	AmqpConsumeAutoAck     bool   `env:"RABBIT_MAILER_AMQP_CONSUME_AUTO_ACK" envDefault:"false"`
	AmqpConsumeExclusive   bool   `env:"RABBIT_MAILER_AMQP_CONSUME_EXCLUSIVE" envDefault:"false"`
	AmqpConsumeNoLocal     bool   `env:"RABBIT_MAILER_AMQP_CONSUME_NO_LOCAL" envDefault:"false"`
	AmqpConsumeNoWait      bool   `env:"RABBIT_MAILER_AMQP_CONSUME_NO_WAIT" envDefault:"false"`
	AmqpConsumeConsumer    string `env:"RABBIT_MAILER_AMQP_CONSUME_CONSUMER" envDefault:""`
	AmqpNackIsMultiple     bool   `env:"RABBIT_MAILER_AMQP_NACK_IS_MULTIPLE" envDefault:"true"`
	AmqpAckIsMultiple      bool   `env:"RABBIT_MAILER_AMQP_ACK_IS_MULTIPLE" envDefault:"true"`
}

func failOnError(err error, msg string, info string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	} else {
		log.Println(info)
	}
}

func getConfig() Config {
	cfg := Config{}
	err := env.Parse(&cfg)
	if err != nil {
		log.Fatalf("%s: %s", "OS env convert error", err)
	}
	return cfg
}

func main() {
	cfg := getConfig()
	conn, err := amqp.Dial(cfg.AmqpConnect)
	failOnError(err, "Failed to connect to RabbitMQ", "Connected to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel", "Channel was opened")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		cfg.AmqpQueueName,
		cfg.AmqpQueueIsDurable,
		cfg.AmqpQueueAutoDelete,
		cfg.AmqpQueueExclusive,
		cfg.AmqpQueueNoWait,
		nil,
	)
	failOnError(err, "Failed to declare a queue", "Queue was declared")

	err = ch.ExchangeDeclare(
		cfg.AmqpExchangeName,
		cfg.AmqpExchangeType,
		cfg.AmqpExchangeIsDurable,
		cfg.AmqpExchangeAutoDelete,
		cfg.AmqpExchangeInternal,
		cfg.AmqpExchangeNoWait,
		nil,
	)
	failOnError(err, "Failed to declare a exchange", "Exchange was declared")

	err = ch.Qos(
		cfg.AmqpPrefetchCount,
		cfg.AmqpPrefetchSize,
		cfg.AmqpQosGlobal,
	)
	failOnError(err, "Failed to set QoS", "QoS was sets")

	msgs, err := ch.Consume(
		q.Name,
		cfg.AmqpConsumeConsumer,
		cfg.AmqpConsumeAutoAck,
		cfg.AmqpConsumeExclusive,
		cfg.AmqpConsumeNoLocal,
		cfg.AmqpConsumeNoWait,
		nil,
	)
	failOnError(err, "Failed to register a consumer", "Consumer was registered")

	forever := make(chan bool)
	go func() {
		cfg := getConfig()
		k := gomail.Dialer{Host: cfg.SmtpHost, Port: cfg.SmtpPort}
		for d := range msgs {
			obj := RabbitMessage{}
			json.Unmarshal(d.Body, &obj)
			if len(obj.Subject) > 0 && len(obj.To) > 0 {
				m := gomail.NewMessage()
				m.SetHeader("From", cfg.SmtpFromEmail)
				m.SetHeader("To", strings.Split(obj.To, ",")...)
				m.SetHeader("Subject", obj.Subject)
				m.SetBody("text/html", obj.Html)
				if len(obj.Cc) > 0 {
					m.SetHeader("Cc", strings.Split(obj.Cc, ",")...)
				}
				if len(obj.Bcc) > 0 {
					m.SetHeader("Bcc", strings.Split(obj.Bcc, ",")...)
				}
				for _, attachment := range obj.Attachments {
					if len(attachment.Filename) != 0 {
						m.Attach(attachment.Filename, gomail.SetCopyFunc(func(w io.Writer) error {
							_, err := w.Write([]byte(attachment.Content))
							return err
						}))
					}
				}
				err := k.DialAndSend(m)
				if err != nil {
					d.Nack(cfg.AmqpNackIsMultiple, true)
					fmt.Printf("%s: %s", "Message return to queue", err)
				} else {
					d.Ack(cfg.AmqpAckIsMultiple)
					fmt.Println("Message was sent")
				}
			} else {
				d.Nack(cfg.AmqpNackIsMultiple, false)
			}
		}
	}()
	log.Printf("[*] Waiting for messages. To exit press CTRL+C")

	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		sig := <-gracefulStop
		fmt.Printf("caught sig: %+v", sig)
		fmt.Println("Wait for 2 second to finish processing")
		time.Sleep(2 * time.Second)
		os.Exit(0)
	}()

	<-forever
}
