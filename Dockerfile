FROM scratch
ADD ca-certificates.crt /etc/ssl/certs/
ADD rabbit-mailer /
CMD ["/rabbit-mailer"]