FILE=rabbit-mailer
COMPILER_FLAGS=CGO_ENABLED=0 GOOS=linux
CONTAINER=rabbit-mailer
CERT=ca-certificates.crt

all:	compile ssl build clean
compile:
	$(COMPILER_FLAGS) go build -a -installsuffix cgo -o $(FILE) .
build:
	docker build -t $(CONTAINER) .
clean:
	rm -rf ./$(FILE) ./$(CERT)
ssl:
	cp /etc/ssl/certs/$(CERT) ./
